<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <meta name="description" content="Smarthr - Bootstrap Admin Template">
        <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern, accounts, invoice, html5, responsive, CRM, Projects">
        <meta name="author" content="Dreamguys - Bootstrap Admin Template">
        <meta name="robots" content="noindex, nofollow">
        <title>Learning Laravel 9 From Basic</title>
        
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/orange/img/favicon.png') }}">
        
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('assets/orange/css/bootstrap.min.css') }}">
        
        <!-- Fontawesome CSS -->
        <link rel="stylesheet" href="{{ asset('assets/orange/css/font-awesome.min.css') }}">
        
        <!-- Lineawesome CSS -->
        <link rel="stylesheet" href="{{ asset('assets/orange/css/line-awesome.min.css') }}">

        @yield('style')
        
        <!-- Main CSS -->
        <link rel="stylesheet" href="{{ asset('assets/orange/css/style.css') }}">
        
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="assets/js/html5shiv.min.js"></script>
            <script src="assets/js/respond.min.js"></script>
        <![endif]-->
        <style>
            .header {
                /* background: linear-gradient(to right, #a10000 0%, #f31330 100%) !important; */
                background:white !important;
                box-shadow: 0 5px 5px 0 rgb(0 0 0 / 30%) !important;
            }
            .header-left {
                background-color: #ab2d16 !important;
            }
            .sidebar {
                background-color: #ab2d16 !important;
            }
            .sidebar-menu li a { 
                color: #e5e5e5 !important;
            }
            .user-menu.nav > li > a {
                color: #000;
            }
            .top-nav-search .form-control { 
                border-color: #b5aeae !important;
                color: #000;
            }
            .top-nav-search .btn {
                color: #000 !important;
            }
            .top-nav-search ::placeholder {
                color: #000 !important;
            }
            .top-nav-search font {
                color: #000 !important;
            }
            .bar-icon span {
                background-color:#000 !important;
            }
            .page-title-box h3 { 
                color: #000 !important;
            }
        </style>
        <!-- Scripts -->
        
    </head>
    <body>
        <!-- Main Wrapper -->
        <div id="app" class="main-wrapper">
        
            <!-- Header -->
            <x-header></x-header>
            <!-- /Header -->
            
            <!-- Sidebar -->
            <x-sidebar></x-sidebar>
            <!-- /Sidebar -->
            
            <!-- Page Wrapper -->
            <div class="page-wrapper">
                {{ $slot }}
            </div>		
            <!-- /Page Wrapper -->
            
            <!-- /Main Wrapper -->
        
        </div>
        <!-- /Main Wrapper -->
        
        <!-- jQuery -->
        <script src="{{ asset('assets/orange/js/jquery-3.2.1.min.js') }}"></script>
        
        <!-- Bootstrap Core JS -->
        <script src="{{ asset('assets/orange/js/popper.min.js') }}"></script>
        <script src="{{ asset('assets/orange/js/bootstrap.min.js') }}"></script>
        
        <!-- Slimscroll JS -->
        <script src="{{ asset('assets/orange/js/jquery.slimscroll.min.js') }}"></script>
        
        @yield('scripts')

        <!-- Custom JS -->
        <script  src="{{ asset('assets/orange/js/app.js') }}"></script>
        
    </body>
</html>