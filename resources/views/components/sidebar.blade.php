<!-- Sidebar -->
<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                <li class="menu-title"> 
                    <span>Main</span>
                </li>
                <li class="{{ (request()->is('/*')) ? 'active' : '' }}"> 
                    <a href=""><i class="la la-dashboard"></i> <span>Dashboard</span></a>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- /Sidebar -->